const express = require('express')
const app = express()

const PORT = 3000

app.listen(PORT, () => console.log(`listening at http://localhost:${PORT}`))

app.use(express.static('public'))

const fruta = {
    codigo: '123',
    descripcion: 'manzana',
    unidad: 1,
    cantidad: 5, 
    proveedor: "tia",
}

const pan = {
    codigo: '12',
    descripcion: 'pan de molde',
    unidad: 1,
    cantidad: 10, 
    proveedor: "comisariato",
}

const jamon = {
    codigo: '11',
    descripcion: 'jamon de pavo',
    unidad: 1,
    cantidad: 2, 
    proveedor: "tia",
}

const leche = {
    codigo: '22',
    descripcion: 'leche de vaca',
    unidad: 1,
    cantidad: 1, 
    proveedor: "comisariato",
}

const cafe = {
    codigo: '33',
    descripcion: 'cafe nacional',
    unidad: 1,
    cantidad: 2, 
    proveedor: "tuti",
}

const azucar = {
    codigo: '44',
    descripcion: 'azucar',
    unidad: 1,
    cantidad: 1, 
    proveedor: "comisariato",
}

const queso = {
    codigo: '55',
    descripcion: 'queso fresco',
    unidad: 1,
    cantidad: 2, 
    proveedor: "tia",
}

const chocolate = {
    codigo: '66',
    descripcion: 'el mejor',
    unidad: 1,
    cantidad: 3, 
    proveedor: "comisariato",
}

const mermelada = {
    codigo: '77',
    descripcion: 'mermelada',
    unidad: 1,
    cantidad: 1, 
    proveedor: "tia",
}

const huevos = {
    codigo: '88',
    descripcion: 'huevos',
    unidad: 1,
    cantidad: 20, 
    proveedor: "comisariato",
}

const fs = require('fs')

const saveData = (data, file) => {
    const finished = (error) => {
        if (error) {
            console.error(error)
            return;
        }
    }
    const jsonData = JSON.stringify(data, null, 2)
    fs.writeFile(file, jsonData, finished)
}

saveData(fruta, 'fruta.json')
saveData(pan, 'pan.json')
saveData(jamon, 'jamon.json')
saveData(leche, 'leche.json')
saveData(cafe, 'cafe.json')
saveData(azucar, 'azucar.json')
saveData(queso, 'queso.json')
saveData(chocolate, 'chocolate.json')
saveData(mermelada, 'mermelada.json')
saveData(huevos, 'huevos.json')